//
//  desafioIOSTests.swift
//  desafioIOSTests
//
//  Created by Ramon on 1/29/16.
//  Copyright © 2016 ramon. All rights reserved.
//

import XCTest
@testable import desafioIOS

class desafioIOSTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWeatherAPI() {
        // functional test case
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let expecItems:XCTestExpectation = self.expectationWithDescription("Load list")
        
        let APIKEY = "cf7767677277bd50ffd0ed2eae07b346"
        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
        var dailyForecast: [DailyForecast]!
        
        let weatherAPI = OWMWeatherAPI(APIKey: APIKEY)
        
        if temperatureUnit == " ℃" { // pega qual a temperatura está sendo usada pelo usuário
            weatherAPI.setTemperatureFormat(kOWMTempCelcius)
        } else {
            weatherAPI.setTemperatureFormat(kOWMTempFahrenheit)
        }
        weatherAPI.setLangWithPreferedLanguage()
        weatherAPI.forecastWeatherByCoordinate(initialLocation.coordinate, withCallback: { (error, results) -> Void in //pega as previsões para uma coordenada
            if error == nil {
                let list = results["list"] as! NSArray //a "list" é onde estão armazenadas as previsões no json enviado
                for dict in list {
                    if dailyForecast == nil {
                        dailyForecast = [DailyForecast(results: dict as! [NSObject : AnyObject], city: (results["city"] as! NSDictionary).objectForKey("name") as! String)]
                    } else {
                        dailyForecast.append(DailyForecast(results: dict as! [NSObject : AnyObject], city: (results["city"] as! NSDictionary).objectForKey("name") as! String))
                    }
                }
                expecItems.fulfill()
            }
        })
        
        self.waitForExpectationsWithTimeout(100, handler: nil)
        
        
    }
    
    func testPerformanceWeatherAPI() {
        //performance test case
        self.measureBlock {
            // Put the code you want to measure the time of here.
            let APIKEY = "cf7767677277bd50ffd0ed2eae07b346"
            let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
            var dailyForecast: [DailyForecast]!
            
            let weatherAPI = OWMWeatherAPI(APIKey: APIKEY)
            
            if temperatureUnit == " ℃" { // pega qual a temperatura está sendo usada pelo usuário
                weatherAPI.setTemperatureFormat(kOWMTempCelcius)
            } else {
                weatherAPI.setTemperatureFormat(kOWMTempFahrenheit)
            }
            weatherAPI.setLangWithPreferedLanguage()
            weatherAPI.forecastWeatherByCoordinate(initialLocation.coordinate, withCallback: { (error, results) -> Void in //pega as previsões para uma coordenada
                if error == nil {
                    let list = results["list"] as! NSArray //a "list" é onde estão armazenadas as previsões no json enviado
                    for dict in list {
                        if dailyForecast == nil {
                            dailyForecast = [DailyForecast(results: dict as! [NSObject : AnyObject], city: (results["city"] as! NSDictionary).objectForKey("name") as! String)]
                        } else {
                            dailyForecast.append(DailyForecast(results: dict as! [NSObject : AnyObject], city: (results["city"] as! NSDictionary).objectForKey("name") as! String))
                        }
                    }
                }
            })

        }
    }
    
}
