//
//  DaysListTableViewController.swift
//  desafioIOS
//
//  Created by Ramon on 1/29/16.
//  Copyright © 2016 ramon. All rights reserved.
//

import UIKit
import CoreLocation

var cities: [String]! //todas as cidades adicionadas pelo usuário
var temperatureUnit = " ℃" // unidade de temperatura escolhida pelo usuário

class DaysListTableViewController: UITableViewController,CLLocationManagerDelegate {
    
    var dailyForecast: [DailyForecast]! //vetor com todas as previsões de tempo
    let locationManager = CLLocationManager()
    var cityCoordinate: CLLocationCoordinate2D! //coordenada da cidade que for escolhida pelo usuário
    var myCoordinate: CLLocationCoordinate2D! // coordenada da cidade do usuário
    let APIKEY = "cf7767677277bd50ffd0ed2eae07b346"
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "City"
        
        let defaults = NSUserDefaults.standardUserDefaults() // recupera do nsuserdefaults todas as cidades que foram adicionadas pelo usuário ou então inicia com um vetor de cidades caso não exista nada
        if let storedCities = defaults.objectForKey("citiesKey") as? [String]
        {
            cities = storedCities
        } else {
            cities = ["Fortaleza", "São Paulo", "São Francisco"]
        }
        
        if let storedTemperatureUnit = defaults.objectForKey("temperatureUnitKey") as? String // recupera a unidade de temperatura escolhida pelo usuário no nsuserdefaults ou então inicia com a unidade de celsius
        {
            temperatureUnit = storedTemperatureUnit
        } else {
            temperatureUnit = " ℃"
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "temperatureUnitDidChange:", name: "temperatureUnitDidChange", object: nil) //observa as notificações para mudança na unidade de temperatura
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "selectedCityDidChange:", name: "selectedCityDidChange", object: nil) //observa as notificações para a mudança na cidade
        
        locationManager.delegate = self // location manager para pegar a localização do usuário
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation() // começa a coletar a localização do usuário
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()

    }
    
    override func viewDidAppear(animated: Bool) {
        tableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func temperatureUnitDidChange (notification: NSNotification) { // se tiver recebido a notificação de mudar temperatura então pega do servidor o json com a temperatura correta
        dailyForecast.removeAll() // limpa o que estava armazenado antes
        navigationController?.popToRootViewControllerAnimated(false)
        getWeatherFromCoordinate(cityCoordinate)
    }
    
    func selectedCityDidChange (notification: NSNotification) { // se tiver recebido a notificação de mudar a cidade então pega do servidor o json com a previsão da cidade correta
        dailyForecast.removeAll() // limpa o que estava armazenado antes
        if let userInfo = notification.userInfo as? Dictionary<String,String> {
            if let cityName = userInfo["cityName"] {
                navigationController?.popToRootViewControllerAnimated(false)
                if cityName == "Minha Localização" {
                    self.getWeatherFromCoordinate(self.myCoordinate)
                } else {
                    let geocoder = CLGeocoder()
                    geocoder.geocodeAddressString(cityName) { (placemark, error) -> Void in
                        if error == nil {
                            self.cityCoordinate = placemark?.last?.location?.coordinate
                            self.getWeatherFromCoordinate(self.cityCoordinate)
                        }
                    }
                }
            }
        }
        
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) { // assim que o location manager obtem a localização do usuário usa as coordenadas para pegar do servidor a previsão da cidade do usuário
        locationManager.stopUpdatingLocation()
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(newLocation) { (placemark, error) -> Void in
            if error == nil {
                self.cityCoordinate = placemark?.last?.location?.coordinate
                self.myCoordinate = self.cityCoordinate
                self.getWeatherFromCoordinate(self.cityCoordinate)
            }
        }
    }
    
    func getWeatherFromCoordinate (coordinate: CLLocationCoordinate2D) { //chama a api do open weather para receber a previsão da coordenada especificada
        let weatherAPI = OWMWeatherAPI(APIKey: self.APIKEY)
        
        if temperatureUnit == " ℃" { // pega qual a temperatura está sendo usada pelo usuário
            weatherAPI.setTemperatureFormat(kOWMTempCelcius)
        } else {
            weatherAPI.setTemperatureFormat(kOWMTempFahrenheit)
        }
        weatherAPI.setLangWithPreferedLanguage()
        weatherAPI.forecastWeatherByCoordinate(coordinate, withCallback: { (error, results) -> Void in //pega as previsões para uma coordenada
            if error == nil {
                let list = results["list"] as! NSArray //a "list" é onde estão armazenadas as previsões no json enviado
                for dict in list {
                    if self.dailyForecast == nil {
                        self.dailyForecast = [DailyForecast(results: dict as! [NSObject : AnyObject], city: (results["city"] as! NSDictionary).objectForKey("name") as! String)]
                    } else {
                        self.dailyForecast.append(DailyForecast(results: dict as! [NSObject : AnyObject], city: (results["city"] as! NSDictionary).objectForKey("name") as! String))
                    }
                }
                self.navigationItem.title = self.dailyForecast[0].locationName
                self.activityIndicator.stopAnimating()
                self.tableView.reloadData()
            }
        })
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if dailyForecast == nil {
            return 1
        } else {
            return dailyForecast.count
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("WheaterCell", forIndexPath: indexPath) as! WheaterCell
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        if dailyForecast == nil {
            return cell
        } else {
            // Configure the cell...
            cell.dayLabel.text = dailyForecast[indexPath.row].dateToString()
            cell.temperatureLabel.text = "Max: " + "\(dailyForecast[indexPath.row].tempHigh.integerValue)" + temperatureUnit + "/ Min: " + "\(dailyForecast[indexPath.row].tempLow.integerValue)" + temperatureUnit
            cell.weatherImage.setImageWithURL(NSURL(string: dailyForecast[indexPath.row].imageNameURL()), usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        performSegueWithIdentifier("detailsView", sender: dailyForecast[indexPath.row])
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "detailsView" {
            if let detailsVC = segue.destinationViewController as? DetailsViewController { //passa a previsão que foi clicada na tabela para a tela de detalhes
                detailsVC.dailyForecast = sender as! DailyForecast
                
            }
        }
    }
    
}
