//
//  CityCell.swift
//  desafioIOS
//
//  Created by Ramon on 1/31/16.
//  Copyright © 2016 ramon. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {

    @IBOutlet var cityName: UILabel!
    @IBOutlet var cityActivationSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
