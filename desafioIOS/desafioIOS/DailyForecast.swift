//
//  DailyForecast.swift
//  desafioIOS
//
//  Created by Ramon on 1/30/16.
//  Copyright © 2016 ramon. All rights reserved.
//

import UIKit

class DailyForecast: NSObject {

    var iconURL = "http://openweathermap.org/img/w/"  //url para pegar as imagens do tempo
    var date: NSDate!
    var humidity: NSNumber!
    var temperature: NSNumber!
    var tempHigh: NSNumber!
    var tempLow: NSNumber!
    var pressure: NSNumber!
    var locationName: String!
    var conditionDescription: String!
    var condition: String!
    var windBearing: NSNumber!
    var windSpeed: NSNumber!
    var icon: String!
    
    init(results: [NSObject : AnyObject], city: String) { //inicia um DailyForecast a partir do dicionario json
        //print(results)
        
        self.date = results["dt"] as! NSDate
        self.humidity = (results["main"] as! NSDictionary).objectForKey("humidity") as! NSNumber
        self.temperature = (results["main"] as! NSDictionary).objectForKey("temp") as! NSNumber
        self.tempHigh = (results["main"] as! NSDictionary).objectForKey("temp_max") as! NSNumber
        self.tempLow = (results["main"] as! NSDictionary).objectForKey("temp_min") as! NSNumber
        self.locationName = city
        self.pressure = (results["main"] as! NSDictionary).objectForKey("pressure") as! NSNumber
        self.conditionDescription = ((results["weather"] as! NSArray).objectAtIndex(0) as! NSDictionary).objectForKey("description") as! String
        self.condition = ((results["weather"] as! NSArray).objectAtIndex(0) as! NSDictionary).objectForKey("main") as! String
        self.icon = ((results["weather"] as! NSArray).objectAtIndex(0) as! NSDictionary).objectForKey("icon") as! String
        self.windBearing = (results["wind"] as! NSDictionary).objectForKey("deg") as! NSNumber
        self.windSpeed = (results["wind"] as! NSDictionary).objectForKey("speed") as! NSNumber
    }
    
    func encodeWithCoder(aCoder: NSCoder!) { 
        aCoder.encodeObject(iconURL, forKey: "iconURL")
        aCoder.encodeObject(date, forKey: "date")
        aCoder.encodeObject(humidity, forKey: "humidity")
        aCoder.encodeObject(temperature, forKey: "temperature")
        aCoder.encodeObject(tempHigh, forKey: "tempHigh")
        aCoder.encodeObject(tempLow, forKey: "tempLow")
        aCoder.encodeObject(pressure, forKey: "pressure")
        aCoder.encodeObject(locationName, forKey: "locationName")
        aCoder.encodeObject(conditionDescription, forKey: "conditionDescription")
        aCoder.encodeObject(condition, forKey: "condition")
        aCoder.encodeObject(windBearing, forKey: "windBearing")
        aCoder.encodeObject(windSpeed, forKey: "windSpeed")
        aCoder.encodeObject(icon, forKey: "icon")
    }
    
    init(coder aDecoder: NSCoder!) {
        self.iconURL = aDecoder.decodeObjectForKey("iconURL") as! String
        self.date = aDecoder.decodeObjectForKey("date") as! NSDate
        self.humidity = aDecoder.decodeObjectForKey("humidity") as! NSNumber
        self.temperature = aDecoder.decodeObjectForKey("temperature") as! NSNumber
        self.tempHigh = aDecoder.decodeObjectForKey("tempHigh") as! NSNumber
        self.tempLow = aDecoder.decodeObjectForKey("tempLow") as! NSNumber
        self.pressure = aDecoder.decodeObjectForKey("pressure") as! NSNumber
        self.locationName = aDecoder.decodeObjectForKey("locationName") as! String
        self.conditionDescription = aDecoder.decodeObjectForKey("conditionDescription") as! String
        self.condition = aDecoder.decodeObjectForKey("condition") as! String
        self.windBearing = aDecoder.decodeObjectForKey("windBearing") as! NSNumber
        self.windSpeed = aDecoder.decodeObjectForKey("windSpeed") as! NSNumber
        self.icon = aDecoder.decodeObjectForKey("icon") as! String
    }
    
    func dateToString() -> String { //converte a nsdate para o formato brasileiro de exibição
        let dateFormatter = NSDateFormatter()
        let brDateFormat = NSDateFormatter.dateFormatFromTemplate("EEEEEE, d MMMM HH:mm", options: 0, locale: NSLocale(localeIdentifier: "pt-BR"))
        dateFormatter.dateFormat = brDateFormat
        let dateString = dateFormatter.stringFromDate(self.date)
        return dateString
    }
    
    func imageNameURL() -> String { //completa a url da imagem para cada previsão de tempo
        return iconURL + icon + ".png"
    }
}
