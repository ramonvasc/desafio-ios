//
//  SettingsTableViewController.swift
//  desafioIOS
//
//  Created by Ramon on 1/31/16.
//  Copyright © 2016 ramon. All rights reserved.
//

import UIKit
import CoreLocation


class SettingsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var temperatureUnitSelection: UISegmentedControl!
    @IBOutlet var tableView: UITableView!
    
    var cells: [CityCell]! //armazena todas as celulas das localidades
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Configurações"
        
        
        if temperatureUnit == " ℃" { //verifica qual a unidade de temperatura está como default
            temperatureUnitSelection.setEnabled(true, forSegmentAtIndex: 0)
        } else if temperatureUnit == " ℉" {
            temperatureUnitSelection.setEnabled(true, forSegmentAtIndex: 1)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectTemperatureUnit(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex { //muda a unidade de temperatura default e armazena no nsuserdefaults
        case 0:
            temperatureUnit = " ℃"
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(temperatureUnit, forKey: "temperatureUnitKey")
            cells.removeAll()
            self.tableView.reloadData()

        case 1:
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(temperatureUnit, forKey: "temperatureUnitKey")
            temperatureUnit = " ℉"
        default:
            break;
        }
        NSNotificationCenter.defaultCenter().postNotificationName("temperatureUnitDidChange", object: nil) // manda a notificação para a tela inicial pegar do servidor as informações com a temperatura correta
    }
    
    @IBAction func addCity(sender: UIBarButtonItem) { //pega um nome e verifica se o nome é de uma cidade, se for adiciona a lista de localidades
        let alertController = UIAlertController(title: "Adicione uma nova cidade", message:
            "Digite o nome da cidade", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Nome da cidade"
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "alertTextFieldDidChange:", name: UITextFieldTextDidChangeNotification, object: textField)
        }
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            let textField = alertController.textFields?.first
            NSNotificationCenter.defaultCenter().removeObserver(self, name: UITextFieldTextDidChangeNotification, object: nil)
            
            let geocoder = CLGeocoder() //através do nome o geocode procura pelas coordenadas da cidade
            geocoder.geocodeAddressString((textField?.text)!) { (placemark, error) -> Void in
                if error == nil {
                    print("Cidade encontrada")
                    cities.append((textField?.text)!) //se a cidade foi encontrada adiciona ela na tabela e salva no nsuserdefaults
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject(cities, forKey: "citiesKey")
                    self.cells.removeAll() //evita adicionar duplicatas do que ja tinha antes
                    self.tableView.reloadData()
                } else {
                    
                    let alertController = UIAlertController(title: "Por favor tente novamente", message:
                        "Não foi possível encontrar a cidade digitada", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
                        print("Cidade não encontrada")
                        
                    }))
                    
                    
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                    
                }
            }
            
            
        }))
        
        
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { action in
            //print("Click of cancel button")
        }))
        alertController.actions.first?.enabled = false
        self.presentViewController(alertController, animated: true, completion: nil)
        
        
    }
    
    func alertTextFieldDidChange (notification: NSNotification)
    {
        if let alertController = self.presentedViewController as? UIAlertController
        {
            let okAction = alertController.actions.first
            okAction?.enabled = true
        }
    }
    
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CityCell", forIndexPath: indexPath) as! CityCell
        // Configure the cell...
        if indexPath.row == 0 { //inicia a tabela sempre com a opção da Minha localização ativada por default e o resto desativado
            cell.cityName.text = "Minha Localização"
            cell.cityActivationSwitch.on = true
        } else {
            cell.cityName.text = cities[indexPath.row]
            cell.cityActivationSwitch.on = false
        }
        
        if cells == nil {
            cells = [cell]
        } else {
            cells.append(cell)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        for (index,cell) in cells.enumerate() { // desativa qualquer localidade que estava seleciona antes e ativa apenas a que foi selecionada pelo usuário
            if index != indexPath.row {
                cell.cityActivationSwitch.on = false
            } else {
                cell.cityActivationSwitch.on = true
                NSNotificationCenter.defaultCenter().postNotificationName("selectedCityDidChange", object: nil, userInfo: ["cityName" : cell.cityName.text!]) //manda uma notificação para a tela principal para ela pegar do servidor a previsão da cidade selecionada
            }
        }
        
    }
    
}
