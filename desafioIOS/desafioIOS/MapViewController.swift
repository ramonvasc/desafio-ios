//
//  MapViewController.swift
//  desafioIOS
//
//  Created by Ramon on 1/29/16.
//  Copyright © 2016 ramon. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet var mapView: MKMapView!
    let regionRadius: CLLocationDistance = 1000000 //raio de visão para a centralização do mapa
    var locationManager = CLLocationManager()



    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locationManager.delegate = self
        mapView.delegate = self
        for city in cities { // adiciona as anottations para cada cidade que o usuário adicionou
            addPinLocationFromCityName(city)
        }
        checkLocationAuthorizationStatus()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //location manager to authorize user location
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    func addPinLocationFromCityName(cityName: String) { // pega o nome da cidade e pega a coordenada com o geocoder para então adicionar uma annotation no mapa
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(cityName) { (placemark, error) -> Void in
            if error == nil {
                let location = placemark?.last?.location
                
                let dropPin = MKPointAnnotation()
                dropPin.coordinate = (location?.coordinate)!
                dropPin.title = cityName
                self.mapView.addAnnotation(dropPin)
            } else {
                print("Cidade não encontrada")
            }
        }

    }
    
    func centerMapOnLocation(location: CLLocation) { //centraliza o mapa perto da localização desejada
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        mapView.showsUserLocation = (status == .AuthorizedWhenInUse)

    }
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) { //centraliza o mapa perto da localização do usuário
        centerMapOnLocation(userLocation.location!)
    }


}
