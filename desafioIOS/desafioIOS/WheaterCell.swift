//
//  WheaterCell.swift
//  desafioIOS
//
//  Created by Ramon on 1/29/16.
//  Copyright © 2016 ramon. All rights reserved.
//

import UIKit

class WheaterCell: UITableViewCell {

    @IBOutlet var weatherImage: UIImageView!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
