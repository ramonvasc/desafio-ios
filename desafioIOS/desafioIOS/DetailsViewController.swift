//
//  DetailsViewController.swift
//  desafioIOS
//
//  Created by Ramon on 1/31/16.
//  Copyright © 2016 ramon. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    var dailyForecast: DailyForecast!
    
    @IBOutlet var tempMax: UILabel!
    @IBOutlet var tempMin: UILabel!
    @IBOutlet var humidity: UILabel!
    @IBOutlet var pressure: UILabel!
    @IBOutlet var windSpeed: UILabel!
    @IBOutlet var weatherImage: UIImageView!
    @IBOutlet var weatherDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = dailyForecast.dateToString()
        tempMax.text = String(dailyForecast.tempHigh.integerValue) + temperatureUnit
        tempMin.text = String(dailyForecast.tempLow.integerValue) + temperatureUnit
        humidity.text = "Humidade: " + String(dailyForecast.humidity.integerValue) + "%"
        pressure.text = "Pressão: " + String(dailyForecast.pressure.floatValue) + " hPa"
        windSpeed.text = "Vento: " + String(dailyForecast.windSpeed.floatValue) + " km/h ESE"
        weatherDescription.text = "Tempo: " + dailyForecast.conditionDescription
        weatherImage.setImageWithURL(NSURL(string: dailyForecast.imageNameURL()), usingActivityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
