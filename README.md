# Criar um aplicativo de consulta a API do [OpenWeatherMap](http://openweathermap.org/) #
Criar um aplicativo para consultar a [API do OpenWeatherMap](http://openweathermap.org/api) e trazer as previsões de tempo. Seguem os mockups para se basear:

![Tela Dias.png](https://bitbucket.org/repo/e68dBG/images/2113844892-Tela%20Dias.png)
![Tela Detalhes.png](https://bitbucket.org/repo/e68dBG/images/2441828634-Tela%20Detalhes.png)
![Tela Mapa.png](https://bitbucket.org/repo/e68dBG/images/2317968612-Tela%20Mapa.png)
![Tela Configurações.png](https://bitbucket.org/repo/e68dBG/images/1408872598-Tela%20Configurac%CC%A7o%CC%83es.png)
## Deve conter ##
* Previsões do tempo de 15 dias da geolocalização do usuário. Exemplo de chamada na API: http://api.openweathermap.org/data/2.5/forecast/daily?lat=35&lon=139&cnt=15&mode=json&appid=2de143494c0b295cca9337e1e96b00e0
* Cada previsão deve exibir ícone do tempo, data, máxima e mínima
* Ao tocar em um item, deve levar para os detalhes da previsão e deve exibir ícone do tempo, data, máxima e mínima, descrição, umidade, pressão, vento
* Mapa com a localização das previsões
* Configurações com opção de alterar para Celsius ou Fahrenheit, opção de adicionar outras localizações.
## Deve conter ##
* Arquivo .gitignore
* Usar Storyboard e Autolayout
* Gestão de dependências no projeto. Ex: Cocoapods
* Framework para Comunicação com API. Ex: AFNetwork
* Mapeamento json -> Objeto . Ex: [Mantle](https://github.com/Mantle/Mantle#mtlmodel)
## Ganha + pontos se conter ##
* Testes unitários no projeto. Ex: XCTests / Specta + Expecta
* Testes funcionais. Ex: KIF
* App Universal , Ipad | Iphone | Landscape | Portrait (Size Classes)
* Cache de Imagens. Ex SDWebImage
## Sugestões ##
As sugestões de bibliotecas fornecidas são só um guideline, sintam-se a vontade para usar diferentes e nos surpreenderem. O importante de fato é que os objetivos macros sejam atingidos. =)
## OBS ##
A foto do mockup é meramente ilustrativa.
## Processo de submissão ##
O candidato deverá implementar a solução e enviar um pull request para este repositório com a solução.

O processo de Pull Request funciona da seguinte maneira:
1. Candidato fará um fork desse repositório (não irá clonar direto!)
1. Fará seu projeto nesse fork.
1. Commitará e subirá as alterações para o SEU fork.
1. Pela interface do Bitbucket, irá enviar um Pull Request.
Se possível deixar o fork público para facilitar a inspeção do código.
## ATENÇÃO ##
Não se deve tentar fazer o PUSH diretamente para ESTE repositório!